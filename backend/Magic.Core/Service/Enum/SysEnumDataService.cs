﻿using Furion;

using Furion.DependencyInjection;
using Furion.DynamicApiController;
using Furion.FriendlyException;
using Furion.RemoteRequest.Extensions;
using Magic.Core.Entity;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using SqlSugar;
using System;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Magic.Core.Service
{
    /// <summary>
    /// 枚举值服务
    /// </summary>
    [ApiDescriptionSettings(Name = "EnumData", Order = 1000)]
    [AllowAnonymous]
    public class SysEnumDataService : ISysEnumDataService, IDynamicApiController, ITransient
    {

        private readonly ISqlSugarClient _sqlSugarClient;

        public SysEnumDataService(ISqlSugarClient sqlSugarClient)
        {
            _sqlSugarClient = sqlSugarClient;
        }
        /// <summary>
        /// 通过枚举类型获取枚举值集合
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [HttpGet("/sysEnumData/list")]
        public async Task<dynamic> GetEnumDataList([FromQuery] EnumDataInput input)
        {
            // 查找枚举
            var enumType = App.EffectiveTypes.FirstOrDefault(t => t.IsEnum && t.Name == input.EnumName);
            if (enumType == null)
                throw Oops.Oh(ErrorCode.D1502).StatusCode(405);

            //// 获取枚举的Key和描述
            return await Task.Run(() =>
                   EnumExtensions.GetEnumDescDictionary(enumType)
                   .Select(x => new EnumDataOutput
                   {
                       Code = x.Key,
                       Value = x.Value
                   }));
        }

        /// <summary>
        /// 通过实体字段类型获取相关集合（目前仅支持枚举类型）
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        [HttpGet("/sysEnumData/listByFiled")]
        public async Task<dynamic> GetEnumDataListByField([FromQuery] QueryEnumDataInput input)
        {
         
            // 获取实体类型属性

            var entityType = Type.GetType($"Magic.Core.Entity.{input.EntityName}");
            if (entityType == null) throw Oops.Oh(ErrorCode.D1504);

            // 获取字段类型
            var fieldType = entityType.GetProperties().FirstOrDefault(p => p.Name == input.FieldName)?.PropertyType;
            if (fieldType is not { IsEnum: true })
                throw Oops.Oh(ErrorCode.D1503);

            // 获取枚举的Key和描述
            return await Task.Run(() =>
                   EnumExtensions.GetEnumDescDictionary(fieldType)
                   .Select(x => new EnumDataOutput
                   {
                       Code = x.Key,
                       Value = x.Value
                   }));
        }
    }
}
