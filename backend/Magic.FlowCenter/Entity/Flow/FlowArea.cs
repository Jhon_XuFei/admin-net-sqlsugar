﻿namespace Magic.FlowCenter
{
	public class FlowArea
    {
        public const string YLANE = "y-lane";   //y泳道
        public const string XLANE = "x-lane";   //x泳道
        public string id { get; set; }

        public string name { get; set; }

        public string type { get; set; }

        public string icon { get; set; }

        public int x { get; set; }
        public int y { get; set; }

        public int width { get; set; }
        public int height { get; set; }
    }
}
