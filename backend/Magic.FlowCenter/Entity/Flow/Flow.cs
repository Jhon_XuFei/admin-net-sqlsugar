﻿using System.Collections.Generic;

namespace Magic.FlowCenter
{
	public class Flow
    {
        public FlowConfig config { get; set; }
        public Attr attr { get; set; }
        public string status { get; set; }
        public List<FlowLine> linkList { get; set; }
        public List<FlowNode> nodeList { get; set; }
        public List<FlowArea> areaList { get; set; }
    }
    public class FlowConfig {
        public bool showGrid { get; set; }
        public string showGridText { get; set; }
        public string showGridIcon { get; set; }
    }
    public class Attr
    {
        public string id { get; set; }
    }
}
