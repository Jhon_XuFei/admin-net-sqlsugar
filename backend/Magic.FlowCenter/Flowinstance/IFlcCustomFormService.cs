﻿using System.Threading.Tasks;

namespace Magic.FlowCenter
{
    public interface  IFlcCustomFormService
    {
        Task Add(long flowInstanceId, string frmData);
        Task Edit(long flowInstanceId, string frmData);
    }
}
