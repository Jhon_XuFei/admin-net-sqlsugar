﻿using Magic.Core;
using System.Collections.Generic;

namespace Magic.FlowCenter
{
	/// <summary>
	/// 工作流输出参数
	/// </summary>
	public class FlcFlowinstanceOutput:FlcFlowinstance
    {
		public string WebId { get; set; }
        public List<FlcFlowInstanceOperationHistory> hisList { get; set; }

    }
}
