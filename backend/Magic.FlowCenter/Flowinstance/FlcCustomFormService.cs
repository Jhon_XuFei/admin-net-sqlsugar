﻿using Furion.DependencyInjection;
using Magic.Core;
using Magic.Core.Entity;
using SqlSugar;
using System;
using System.Threading.Tasks;
using Yitter.IdGenerator;

namespace Magic.FlowCenter
{
	public class FlcCustomFormService : IFlcCustomFormService, ITransient
    {
        private readonly ISqlSugarRepository<FlcCustomForm> _rep;

        public FlcCustomFormService(ISqlSugarRepository<FlcCustomForm> rep)
        {
            _rep = rep;
        }
        public async Task Add(long flowInstanceId, string frmData)
        {
            var req = frmData.ToObject<FlcCustomForm>();
            req.FlowInstanceId = flowInstanceId;
            req.Id = YitIdHelper.NextId();
            req.CreatedUserId = UserManager.UserId;
            req.CreatedUserName = UserManager.Name;
            req.CreatedTime = DateTime.Now;
            await _rep.InsertAsync(req);
        }
        public async Task Edit(long flowInstanceId, string frmData)
        {
            var req = frmData.ToObject<FlcCustomForm>();
            req.FlowInstanceId = flowInstanceId;
            await _rep.Context.Updateable<FlcCustomForm>(a => new FlcCustomForm
            {
                Remark = a.Remark,
                Sort = a.Sort,
                Name = a.Name

            }).Where(a => a.FlowInstanceId == req.FlowInstanceId).ExecuteCommandAsync();
        }
    }
}
